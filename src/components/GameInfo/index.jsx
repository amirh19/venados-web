import React from "react";
import logo from "../../assets/logo.png";
import "./style.css";
const GameInfo = (props) => (
  <div className="row">
    <div className="grass game-info offset-md-2 col-md-8 col-12">
      <div className="row">
        <div className="right col-6">
          <span className="one-per-line">
            <i className="far fa-calendar-alt"></i>
            <br />
            {`${props.gameDate.getDate()} ${props.gameDate
              .toLocaleDateString("es", { weekday: "long" })
              .substr(0, 3)
              .toUpperCase()}`}
          </span>
          <img
            className="fc-logo"
            src={props.game.local ? logo : props.game.opponent_image}
            alt="Local"
          />
        </div>
        <div className="left col-6">
          <span>{`${props.game.home_score} - ${props.game.away_score}`}</span>
          <img
            alt="Visitor"
            className="fc-logo"
            src={!props.game.local ? logo : props.game.opponent_image}
          />
        </div>
      </div>
    </div>
  </div>
);

export default GameInfo;
