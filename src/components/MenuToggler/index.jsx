import React from "react";
import "./style.css";
const MenuToggler = (props) => (
  <button className="toggler" onClick={props.onClick}>
    <i className="fa fa-bars"></i>
  </button>
);
export default MenuToggler;
