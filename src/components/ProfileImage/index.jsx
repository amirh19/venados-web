import React from "react";
import "./style.css";
const ProfileImage = (props) => (
  <img className="profile" src={props.source} alt="Profile" />
);

export default ProfileImage;
