import React from "react";
import "./style.css";
const Loader = (props) => (
  <div className="loader">
    <i className="fas fa-spinner fa-spin"></i>
  </div>
);
export default Loader;
