import React from "react";
import "./style.css";
import logo from "../../assets/logo.png";

const Shield = (props) => (
  <div className="shield">
    <img src={logo} alt="Shield" />
  </div>
);

export default Shield;
