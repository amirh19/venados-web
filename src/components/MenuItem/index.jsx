import React from "react";
import { NavLink } from "react-router-dom";
import "./style.css";

const MenuItem = (props) => (
  <NavLink
    exact
    to={props.link}
    className="menu-item"
    activeClassName="active-tab"
  >
    {props.text}
  </NavLink>
);
export default MenuItem;
