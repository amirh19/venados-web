import React from "react";
import "./style.css";
const Modal = (props) => (
  <div className={`modal-background ${props.active ? "active" : ""}`}>
    <div className="modal">
      <i onClick={props.onClose} className="far fa-times-circle"></i>
      {props.children}
    </div>
  </div>
);
export default Modal;
