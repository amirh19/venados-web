import React from "react";
import "./style.css";
const PlayerCard = (props) => (
  <div className="card">
    <div className="card-header">
      <span className="card-title">{props.title}</span>
      <img className="player" src={props.image} alt="Player" />
      <span className="name">{props.name}</span>
      <span className="position">{props.position}</span>
    </div>
    <div className="card-body">
      {props.info.map((item, index) => (
        <React.Fragment key={index}>
          <span className="card-info">{item.title}</span>
          <span className="info">{item.description}</span>
        </React.Fragment>
      ))}
    </div>
  </div>
);
export default PlayerCard;
