import React from "react";
import "./style.css";

const Player = (props) => (
  <div onClick={props.onClick} className="player col-lg-2 col-md-3 col-4">
    <img className="player" src={props.player.image} alt="player" />
    {props.player.position && <span>{props.player.position}</span>}
    {props.player.role && <span>{props.player.role}</span>}
    <span className="player-name">
      {`${props.player.name.split(" ")[0]} ${props.player.first_surname}`}
    </span>
  </div>
);
export default Player;
