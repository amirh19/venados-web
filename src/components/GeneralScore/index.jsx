import React from "react";
import "./style.css";

const GeneralScore = (props) => (
  <div className="row">
    <div className="club offset-md-2 col-md-8 col-12">
      <span>{props.position}</span>
      <img className="logo" src={props.logo} alt="club's logo" />
      <span className="team">{props.team}</span>
      <span>{props.games}</span>
      <span>{props.scoreDiff}</span>
      <span>{props.points}</span>
    </div>
  </div>
);

export default GeneralScore;
