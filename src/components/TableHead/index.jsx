import React from "react";
import "./style.css";
const TableHead = (props) => (
  <div className="row">
    <div className="offset-md-2 col-md-8 col-12 head">
      <span className="title">{props.text}</span>
      {props.cells &&
        props.cells.map((cell) => <span className="short">{cell}</span>)}
    </div>
  </div>
);
export default TableHead;
