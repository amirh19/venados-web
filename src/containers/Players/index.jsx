import React, { Component } from "react";
import axios from "axios";
import Player from "../../components/Player";
import PlayerCard from "../../components/PlayerCard";
import Modal from "../../components/Modal";
import Loader from "../../components/Loader";

class Players extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      team: {},
      showModal: false,
      currentPlayer: false,
    };
  }
  async componentDidMount() {
    try {
      const res = await axios.get(process.env.REACT_APP_API + "players", {
        headers: {
          "Content-Type": "application/json",
          accept: "application/json",
        },
      });
      console.log(res.data.data);
      let team = res.data.data.team;
      this.setState({ team: team });
    } catch (error) {
      console.error(error);
    } finally {
      this.setState({ loading: false });
    }
  }

  /**
   * @typedef {Object} Player
   * @property {string} name player's name
   * @property {string} image player's picture
   * @property {string} first_surname players first surname
   * @property {string} second_surname player's second surname
   * @property {number|null} height
   * @property {number|null} weight
   * @property {string|null} last_team player's last team
   */

  createPlayers = () => {
    let players = [];
    for (const position in this.state.team) {
      this.state.team[position].forEach(
        /**
         * @param {Player} player Player info
         * @param {number} index
         */
        (player, index) =>
          players.push(
            <Player
              onClick={() => this.showPlayerInfo(player)}
              key={`${index}-${position}`}
              player={player}
            />
          )
      );
    }
    return players;
  };
  /**
   * @description function used to show modal
   * @param {Player} player
   */
  showPlayerInfo = (player) => {
    this.setState({
      showModal: true,
      currentPlayer: player,
    });
  };

  render() {
    return (
      <>
        {this.state.loading && <Loader />}
        <div className="row">{this.createPlayers()}</div>
        {this.state.currentPlayer && (
          <Modal
            onClose={() => this.setState({ showModal: false })}
            active={this.state.showModal}
          >
            <PlayerCard
              title="FICHA TÉCNICA"
              player={this.state.currentPlayer}
              image={this.state.currentPlayer.image}
              position={this.state.currentPlayer.position}
              info={[
                {
                  title: "FECHA DE NACIMIENTO",
                  description: new Date(this.state.currentPlayer.birthday)
                    .toLocaleString("es-MX")
                    .substr(0, 9),
                },
                {
                  title: "PESO",
                  description: this.state.currentPlayer.height,
                },
                {
                  title: "ALTURA",
                  description: this.state.currentPlayer.weight,
                },
                {
                  title: "EQUIPO ANTERIOR",
                  description: this.state.currentPlayer.last_team || "",
                },
              ]}
              name={`${this.state.currentPlayer.name} ${this.state.currentPlayer.first_surname} ${this.state.currentPlayer.second_surname}`}
            />
          </Modal>
        )}
      </>
    );
  }
}
export default Players;
