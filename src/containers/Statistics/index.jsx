import React, { Component } from "react";
import axios from "axios";
import GeneralScore from "../../components/GeneralScore";
import Loader from "../../components/Loader";
import TableHead from "../../components/TableHead";
class Statistics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      statistics: [],
    };
  }
  async componentDidMount() {
    try {
      let res = await axios.get(process.env.REACT_APP_API + "statistics", {
        headers: {
          "Content-Type": "application/json",
          accept: "application/json",
        },
      });
      let statistics = res.data.data.statistics;
      //sorting, just in case response is not sorted.
      statistics = statistics.sort((a, b) => a.position - b.position);
      this.setState({ statistics: statistics });
    } catch (error) {
      console.error(error);
    } finally {
      this.setState({ loading: false });
    }
  }

  render() {
    return (
      <>
        {this.state.loading && <Loader />}
        <TableHead text="Tabla General" cells={["JJ", "DG", "PTS"]} />
        {this.state.statistics.map((club, index) => (
          <GeneralScore
            key={index}
            position={club.position}
            logo={club.image}
            team={club.team}
            scoreDiff={club.score_diff}
            games={club.games}
            points={club.points}
          />
        ))}
      </>
    );
  }
}
export default Statistics;
