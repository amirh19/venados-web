import React, { Component } from "react";
import logo from "../../assets/logo.png";
import MenuItem from "../../components/MenuItem";
import MenuToggler from "../../components/MenuToggler";
import ProfileImage from "../../components/ProfileImage";
import "./style.css";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
    };
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.closeNavBar);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.closeNavBar);
  }

  /**
   * @description function used to toggle navbar button on mobile devices.
   */
  toggleNavBar = () => {
    this.setState((state) => ({ showMenu: !state.showMenu }));
  };

  /**
   * @description function used to close navbar on outside tap.
   * @param {EventListener} e;
   */
  closeNavBar = (e) => {
    if (!this.node.contains(e.target)) {
      this.setState({ showMenu: false });
    }
  };

  render() {
    return (
      <div className="row" ref={(node) => (this.node = node)}>
        <div className="col-12">
          <MenuToggler onClick={this.toggleNavBar} />
          <div className={`menu ${this.state.showMenu ? "show" : ""}`}>
            <ProfileImage source={logo} />
            <MenuItem link="/" text="Home" />
            <MenuItem link="/statistics" text="Estadísticas" />
            <MenuItem link="/players" text="Jugadores" />
          </div>
        </div>
      </div>
    );
  }
}
export default NavBar;
