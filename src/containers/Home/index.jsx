import React, { Component } from "react";
import axios from "axios";
import "./style.css";
import GameInfo from "../../components/GameInfo";
import Loader from "../../components/Loader";
import TableHead from "../../components/TableHead";
import Shield from "../../components/Shield";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      ASC: {},
      MX: {},
      active: 1,
    };
  }
  async componentDidMount() {
    try {
      this.setState({ loading: true });
      //get games from API
      let res = await axios.get(process.env.REACT_APP_API + "games", {
        headers: {
          "Content-Type": "application/json",
          accept: "application/json",
        },
      });
      let games = res.data.data.games;
      //filter games by league
      const MX = this.filterByMonth(
        games.filter((game) => game.league === "Copa MX")
      );
      const ASC = this.filterByMonth(
        games.filter((game) => game.league === "Ascenso MX")
      );
      //save into state
      this.setState({
        MX: MX,
        ASC: ASC,
      });
    } catch (error) {
      console.error(error);
    } finally {
      this.setState({ loading: false });
    }
  }
  /**
   * @description function used to filter games by month
   * @param {Object[]} games
   * @returns {Object} Each key is an Array of Games
   */
  filterByMonth = (games) => {
    if (!Array.isArray(games))
      throw new TypeError("Param expected is an array");
    try {
      let filtered = {};
      let aux;
      while (games.length > 0) {
        let firstElementMonth = new Date(games[0].datetime).getMonth();
        aux = games.filter((game) => {
          let gameDate = new Date(game.datetime).getMonth();
          return gameDate === firstElementMonth;
        });
        // Expecting that all the games are in the same year or only provided a one year period.
        filtered[firstElementMonth] = aux;
        games = games.filter((game) => {
          let gameDate = new Date(game.datetime).getMonth();
          return gameDate !== firstElementMonth;
        });
      }
      return filtered;
    } catch (error) {
      console.error(error);
      return {};
    }
  };

  /**
   * @description Function used to generate table, divided by months
   * @param {Object} league Each key represents the number of the month (0-11) and contains an Array of
   * games that were played in that month.
   *
   */

  getTable = (league) => {
    const MONTH = {
      0: "ENERO",
      1: "FEBRERO",
      2: "MARZO",
      3: "ABRIL",
      4: "MAYO",
      5: "JUNIO",
      6: "JULIO",
      7: "AGOSTO",
      8: "SEPTIEMBRE",
      9: "OCTUBRE",
      10: "NOVIEMBRE",
      11: "DICIEMBRE",
    };
    let rows = [];
    for (let month in league) {
      //Create elements
      rows.push(
        <React.Fragment key={month}>
          <TableHead text={MONTH[month]} />
          {league[month].map((game, index) => {
            let gameDate = new Date(game.datetime);
            return (
              <GameInfo
                index={index}
                month={month}
                game={game}
                gameDate={gameDate}
                key={index}
              />
            );
          })}
        </React.Fragment>
      );
    }
    return rows;
  };

  render() {
    return (
      <>
        {this.state.loading && <Loader />}

        <div className="container">
          <div className="row">
            <Shield />
          </div>
          <div className="row">
            <div
              className={`tab offset-md-2 col-md-4 col-6 ${
                this.state.active === 1 ? "active" : ""
              }`}
              onClick={() => this.setState({ active: 1 })}
            >
              COPA MX
            </div>
            <div
              className={`tab col-md-4 col-6 ${
                this.state.active === 2 ? "active" : ""
              }`}
              onClick={() => this.setState({ active: 2 })}
            >
              ASCENSO MX
            </div>
          </div>
          {this.state.active === 1 && this.getTable(this.state.MX)}
          {this.state.active === 2 && this.getTable(this.state.ASC)}
        </div>
      </>
    );
  }
}
export default Home;
