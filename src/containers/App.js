import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NavBar from "./NavBar";
import Statistics from "./Statistics";
import Players from "./Players";
import Home from "./Home";
function App() {
  return (
    <div className="container">
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/statistics">
            <Statistics />
          </Route>
          <Route exact path="/players">
            <Players />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
